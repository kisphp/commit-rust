.PHONY: help build

.DEFAULT_GOAL := help

help: ## Display this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "[36m%-30s[0m %s\n", $$1, $$2}'

special: ## build for mac arm64
	rustup target add aarch64-apple-darwin
	cargo build --release --target aarch64-apple-darwin
	sudo mv target/aarch64-apple-darwin/release/commit /usr/local/bin/commit
#	rustup target add x86_64-apple-darwin
	#cargo build --release --target x86_64-apple-darwin

build: ## Build and deploy executable for Darwin Intel amd64`
	cargo build --release
	sudo mv target/release/commit /usr/local/bin/commit
