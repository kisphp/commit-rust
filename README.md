# Commit CLI

Commit CLI is a Rust command-line application that simplifies the process of making Git commits with various options.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
    - [Basic Commit](#basic-commit)
    - [Commit and Stage](#commit-and-stage)
    - [Commit, Stage, and Push](#commit-stage-and-push)

## Installation

Before using Commit CLI, ensure that you have Rust and Cargo installed on your system. You can download and install them from [the official Rust website](https://www.rust-lang.org/tools/install).

To build Commit CLI:

```sh
cargo build --release
```

After building, you can find the executable in the `target/release` directory.

Once you have the binary file compiled, run the following commands to move it to binaries directory:

```shell
# make file executable
chmod +x target/release/commit

# move file to binaries directory
sudo mv target/release/commit /usr/local/bin/commit
```

## Usage
Commit CLI simplifies Git commit operations with the following options:

### Basic Commit

To create a basic Git commit with a message, use the following command:

```sh
commit <message>
```
Replace <message> with your commit message. For example:

```sh
commit Fixed a bug
```
> Note that quotation around the message text are not mandatory

### Commit and Stage
To stage all changes using `git add .` before committing, use the `-a` flag:

```sh
commit -a <message>
```
For example:

```sh
commit -a Added new feature
```

### Commit, Stage, and Push
To stage changes, commit, and push to the remote repository, use the `-ap` flags:

```sh
commit -ap <message>
```
For example:

```sh
commit -ap Updated documentation
```

### Debug messages

You can show more information while the application is running by adding the `-d` flag:

```shell
commit this is my message -apd
```

### Run `git status` at the end

You also can run `git status` at the end of execution just by adding the flag `-s`:

```shell
commit this is my message -aps
```

Please note that you should always ensure that you are in a Git-initialized directory with the appropriate remote set up before using the `-ap` option.

After running any of the above commands, the Commit CLI will execute the corresponding Git commands and display whether the Git operation was successful.

Enjoy using Commit CLI to streamline your Git commit workflow!
