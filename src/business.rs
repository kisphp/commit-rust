extern crate regex;

use regex::Regex;
use std::process::Command;
use colored::Colorize;

struct OutputPainter;

impl OutputPainter {
    fn error(message: &str) {
        println!("\n{}\n", message.red());
    }
    fn info(message: &str) {
        println!("{}", message.blue());
    }
    fn debug(message: &str) {
        println!("{}", message.magenta().italic());
    }
    fn warning(message: &str) {
        println!("{}", message.color("lightblue"));
    }
    fn success(message: &str) {
        println!("{}", message.green());
    }
}

fn count_staged_files() -> usize {
    let output = Command::new("git")
        .args(&["diff", "--cached", "--numstat"])
        .output()
        .expect("Failed to execute git command");

    let output_str = String::from_utf8_lossy(&output.stdout);
    output_str.lines().count()
}

fn get_git_branch_name() -> Option<String> {
    let status_output = Command::new("git")
        .arg("branch")
        .arg("--show-current")
        .output()
        .expect("Failed to run git command");

    let branch_name = String::from_utf8_lossy(&status_output.stdout);

    Some(branch_name.to_string())
}

fn get_ticket_id(branch_name: &str) -> Option<String> {
    // Create a regular expression pattern to match the substring
    let pattern = Regex::new(r"([a-zA-Z]{2,})\-([\d]+)").unwrap();

    // Find the first match of the pattern in the string
    if let Some(matched) = pattern.find(branch_name) {
        // Get the matched substring
        let matched_str = matched.as_str().to_string();

        return Some(matched_str);
    }

    None
}

fn run_git_commit(message: &str) -> bool {
    // Run "git commit -m <message>" command
    let output = Command::new("git")
        .arg("commit")
        .arg("-m")
        .arg(message)
        .output()
        .expect("Failed to run commit command");

    output.status.success()
}

pub fn execute(message: Vec<String>, add: bool, push: bool, cmd_status: bool, debug: bool) {
    if debug {
        OutputPainter::debug(format!("message: {:?}", message).as_str());
        OutputPainter::debug(format!("stage: {:?}", add).as_str());
        OutputPainter::debug(format!("push: {:?}", push).as_str());
        OutputPainter::debug(format!("status: {:?}", cmd_status).as_str());
        OutputPainter::debug(format!("debug: {:?}", debug).as_str());
    }

    if let Some(branch_name) = get_git_branch_name() {
        if add {
            if debug {
                OutputPainter::debug("Running git add .");
            }
            Command::new("git")
                .arg("add")
                .arg(".")
                .output()
                .expect("Failed to run git add");
        }

        let count_files = count_staged_files();
        if count_files < 1 {
            OutputPainter::error("No files staged in git. Run 'git add .' before, or add '-a' flag.");
            return;
        }

        OutputPainter::info(format!("Current git branch is: {}", branch_name.bold()).as_str());

        let mut git_commit_message = message.clone()[1..].join(" ").to_string();

        if let Some(ticket_id) = get_ticket_id(&branch_name) {
            OutputPainter::info(format!("Ticket ID is: {}", ticket_id.bold()).as_str());
            git_commit_message.insert_str(0, &(ticket_id.as_str().to_owned() + " "));
        } else {
            OutputPainter::warning("Ticket ID not found in branch name")
        }
        OutputPainter::success(format!("Git commit message: {}", git_commit_message.bold()).as_str());
        run_git_commit(&git_commit_message);

        if push {
            if debug {
                OutputPainter::debug("Runnung: git push");
            }
            let push_output = Command::new("git")
                .arg("push")
                // .arg("-u")
                // .arg("origin")
                // .arg(&branch_name.to_string().trim())
                .output()
                .expect("Failed to run git push");

            // println!("push error: {:?}", String::from_utf8_lossy(&push_output.stderr));
            println!("push output: {}", String::from_utf8_lossy(&push_output.stdout));
        }
        if cmd_status {
            if debug {
                OutputPainter::debug("Runnung: git status");
            }
            let status_output = Command::new("git")
                .arg("status")
                .output()
                .expect("Failed to run git status");

            println!("status output: {}", String::from_utf8_lossy(&status_output.stdout));
        }
    } else {
        OutputPainter::error("Not in a GIT repository");
    }
}

