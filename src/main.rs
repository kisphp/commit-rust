mod business;

use clap::{Parser};
use business::execute;

#[derive(Parser)]
#[command(author, version, about="A custom git commit command", long_about = None)]
#[command(next_line_help = true)]
#[command(arg_required_else_help = true)]
struct Commit {
    /// The commit message to be added for the changed files
    message: Vec<String>,

    /// Stage all changes using 'git add .' before commit
    #[arg(short, long)]
    add: bool,

    /// Push the committed changes to the remote repository
    #[arg(short, long)]
    push: bool,

    /// Run 'git status' after command execution
    #[arg(short, long)]
    status: bool,

    /// Show debug messages
    #[arg(short, long)]
    debug: bool,
}

fn main() {
    let cli = Commit::parse();
    //
    // println!("message: {:?}", cli.message);
    // println!("stage: {:?}", cli.add);
    // println!("push: {:?}", cli.push);

    execute(cli.message, cli.add, cli.push, cli.status, cli.debug);
}
